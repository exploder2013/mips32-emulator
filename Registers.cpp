#include "Registers.h"

#ifdef _DEBUG
	#define DBGPRINT( text ) printf( "FILE: %s (%d line): %s\n",  __FUNCTION__, __LINE__, text )
	#define DBGPRINT_STRING( text, arg ) printf( "FILE: %s (%d line): %s: %s!\n",  __FUNCTION__, __LINE__, text, arg )
	#define DBGPRINT_INT( text, arg ) printf( "FILE: %s (%d line): %s: %d!\n",  __FUNCTION__, __LINE__, text, arg )
#else
	#define DBGPRINT( text )
	#define DBGPRINT_STRING( text, arg )
	#define DBGPRINT_INT( text, arg )
#endif

MIPS32_Registers::MIPS32_Registers()
{
	const std::string registerNames[] = { "zero", "at", "v0", "v1", "a0", "a1", "a2", "a3", "t0",
	 "t1", "t2", "t3", "t4", "t5", "t6", "t7", "t8", "t9", "s0", "s1", "s2", "s3", "s4", "s5", "s6", 
	 "s7", "k0", "k1", "gp", "sp", "fp", "ra" };
	
	int counter = 0;

	/* assign the names to the registers */
	for( auto reg : registerNames ) 
	{
		registers[ counter ].name = reg;
		counter++;
	}
		
}

MIPS32_Registers::MIPS32_Registers( const MIPS32Register _registers[ 32 ] )
{
	/* setup the names of the registers */
	MIPS32_Registers();

	/* copy the startup values to the registers */
	for( int i = 0; i < 32; i++ )
		registers[ i ].value = _registers[ i ].value;
}

int MIPS32_Registers::getRegisterValue( const std::string & registerName )
{
	PMIPS32Register reg = findRegister( registerName );
	if( reg != nullptr )
		return reg->value;

	return 0;
}

MIPS32Register* MIPS32_Registers::getRegister( const std::string & registerName )
{
	PMIPS32Register reg = findRegister( registerName );
	return reg;
}

MIPS32Register* MIPS32_Registers::getRegister( const int & registerIndex )
{
	if( registerIndex > 31 || registerIndex < 0 )
	{
		DBGPRINT_INT( "invalid register index", registerIndex );
		return nullptr;
	}

	return &registers[ registerIndex ];
}

std::array< MIPS32Register, 32 > MIPS32_Registers::getRegisters()
{
	return registers;
}

MIPS32Register* MIPS32_Registers::findRegister( const std::string name )
{
	const std::string registerNames[] = { "zero", "at", "v0", "v1", "a0", "a1", "a2", "a3", "t0",
	"t1", "t2", "t3", "t4", "t5", "t6", "t7", "t8", "t9", "s0", "s1", "s2", "s3", "s4", "s5", "s6",
	"s7", "k0", "k1", "gp", "sp", "fp", "ra" };

	std::string nameLower = name;

	if( name.empty() )
	{
		DBGPRINT( "provided register name was empty!" );
		return nullptr;
	}

	/* convert provided register name to lowercase */
	for( unsigned int i = 0; i < nameLower.length(); i++ )
		nameLower[ i ] = static_cast<char>( std::tolower( nameLower[i] ) );

	int counter = 0;
	for( ; counter < 32; counter++ )
	{
		if( registerNames[counter] == nameLower )
			return &registers[ counter ];
	}

	DBGPRINT_STRING( "provided register name was not found", name.c_str() );
	return nullptr;
}

MIPS32_Registers::~MIPS32_Registers()
{
}
