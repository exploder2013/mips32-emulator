#include "FileLoader.h"

#define DBGPRINT( text ) printf( "FILE: %s (%d line): %s\n",  __FUNCTION__, __LINE__, text )

MIPS32_FileLoader::MIPS32_FileLoader( const std::string& fileLocation )
{
	if( !fileLocation.empty() )
	{
		file = std::ifstream( fileLocation );
		if( file.is_open() ){
			processFile();
		} else {
			DBGPRINT( "nonexistant file provided!" );
		}
	} else {
		DBGPRINT( "empty file provided!" );
	}
}

MIPS32_FileLoader::MIPS32_FileLoader()
{
}

MIPS32_FileLoader::~MIPS32_FileLoader()
{
	if( file.is_open() )
		file.close();
}

std::vector<std::string> MIPS32_FileLoader::getProcessedFileLines()
{
	return processedLines;
}

bool MIPS32_FileLoader::openFile( const std::string& path )
{
	if( path.empty() )
		return false;

	file = std::ifstream( path );
	if( file.is_open() )
		return true;
	
	return false;
}

void MIPS32_FileLoader::startProcessing()
{
	if( file.is_open() )
	{
		processFile();
	} else {
		DBGPRINT( "nonexistant file provided!" );
	}
}

void MIPS32_FileLoader::processFile()
{
	std::string line;
	std::string code;
	std::string nextLine;

	if( !file.is_open() )
		return;

	/* File is open, process the lines in it. */
	while( getline( file, line ) )
	{
		/* if it is a comment then don't process it */
		if( isComment( line ) )
			continue;

		code			= remoteWhitespace( line );
		code			= parseCode( code );
		auto finalCode	= splitCode( code );

		for( auto s : finalCode )
			processedLines.push_back( s );
	}

	file.close();
}

bool MIPS32_FileLoader::isComment( const std::string & line )
{
	/* Consider an empty line a comment */
	if( line.empty() ) 
	{
		return true;
	/* Consider an empty line with newline char a comment also */
	} else if( line.at( 0 ) == '\n' )
	{
		return true;
	}
		

	for( unsigned int i = 0; i < line.length(); i++ )
	{
		char character = line.at( i );

		// Skip all the whitespace
		if( character == ' ' )
		{
			continue;
		// # character indicates a comment
		} else if( character == '#' )
		{
			return true;
		// spaced newlines
		} else if( character == '\n' )
		{
			return true;
		// tab character
		} else if( character == '\t') 
		{
			continue;
		// some other character than these (indicates not a comment)
		} else
		{
			return false;
		}
	}

	/* Contains only spaces or \t characters */
	return true;
}

std::string MIPS32_FileLoader::remoteWhitespace( const std::string & line )
{
	const std::string whitespaceChars	= " \t";
	const std::string commentChars		= "#";

	std::string processedLine 	= line;
	int			commentStart	= 0;

	/* Match and remove all the leading spaces */
	processedLine.erase( 0, processedLine.find_first_not_of( whitespaceChars ) );
	commentStart = processedLine.find_first_of( commentChars );

	/* Match and remove all the spaces before the comment ( -1 == not found ) */
	if( commentStart != -1 )
	{
		char character	= processedLine.at( commentStart - 1 );
		int position	= 0;
		
		/* get all of the spaces after the arguments */
		while( character == whitespaceChars[0] || character == whitespaceChars[1] )
		{
			position++;
			character = processedLine.at( commentStart - 1 - position );
		}

		/* if there is space before the comment, then it can't be the space between arguments */
		character = processedLine.at( commentStart - position );
		if( character == whitespaceChars[0] )
		{
			processedLine.erase( commentStart - position, processedLine.rfind( whitespaceChars ) );
		} else if( character == whitespaceChars[1] )
		{
			processedLine.erase( commentStart - position, processedLine.rfind( whitespaceChars ) );
		}
	}

	return processedLine;
}

std::string MIPS32_FileLoader::parseCode( const std::string & line )
{
	const std::string matchChars		= ", ";
	std::string processedLine			= line;

	unsigned int found = 0;

	/* Get rid of formating spaces */
	while( ( found = processedLine.find( matchChars ) ) != std::string::npos )
		processedLine = processedLine.replace( found, matchChars.length(), "," );

	/* Get rid of tab chars and replace with spaces */
	while( ( found = processedLine.find_first_of( '\t' ) ) != std::string::npos )
		processedLine = processedLine.replace( found, 1, " " );		

	/* Get rid of double spaces (very slow, should improve)*/
	while( ( found = processedLine.find( "  " ) ) != std::string::npos )
	{
		int count = 2;

		/* add all extra whitespaces after these (saves a lot of performance) */
		while( ( found + count - 1 ) < processedLine.length() )
		{
			if( processedLine.at( found + count - 1 ) == ' ' ) {
				count++;
			} else {
				break;
			}
				
		}

		if( found + count == processedLine.length() + 1 )
		{
			processedLine = processedLine.replace( found, count - 1, "" );
		} else {
			processedLine = processedLine.replace( found, count - 1, " " );
		}
	}
		
		
	return processedLine;
}

std::vector<std::string> MIPS32_FileLoader::splitCode( const std::string & line )
{
	std::vector< std::string > split;

	unsigned int pos = 0;

	/* label */
	if( ( pos = line.find_first_of( ":" ) ) != -1 )
	{
		pos++;

		/* variable, not label */
		if( line.find_first_of( "." ) != -1 )
		{
			split.push_back( line );
			return split;
		}
		
		/* label without any code */
		if( pos >= line.length() )
		{
			split.push_back( line );
			return split;
		}

		while( pos < line.length() && line.at( pos ) == ' ' )
			pos++;

		if( pos >= line.length() )
		{
			split.push_back( line );
			return split;
		}

		if( pos < line.length() && line.at( pos ) != '\n' )
		{
			unsigned int charPos = line.find_first_of( ":" );

			split.push_back( line.substr( 0, charPos + 1 ) );

			/* remove spaces */
			charPos++;
			while( charPos < line.length() && line.at( charPos ) == ' ' )
				charPos++;

			split.push_back( line.substr( charPos, line.length() - charPos ) );
		}
	} else {
		split.push_back( line );
	}

	return split;
}
