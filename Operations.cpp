#include "Operations.h"

MIPS32_Operations::MIPS32_Operations()
{
}

MIPS32_Operations::~MIPS32_Operations()
{
}

/*
MIPS32_Operations * MIPS32_Operations::getInstance()
{
	if( !instance )
		instance = new MIPS32_Operations();

	return instance;
}
*/

void MIPS32_Operations::op_add( MIPS32Register & dest, const MIPS32Register & reg1, const MIPS32Register & reg2 )
{
	dest.value = reg1.value + reg2.value;
}

void MIPS32_Operations::op_add( MIPS32Register & dest, const MIPS32Register & reg1, const int c1 )
{
	dest.value = reg1.value + c1;
}

void MIPS32_Operations::op_sub( MIPS32Register & dest, const MIPS32Register & reg1, const MIPS32Register & reg2 )
{
	dest.value = reg1.value - reg2.value;
}

void MIPS32_Operations::op_sub( MIPS32Register & dest, const MIPS32Register & reg1, const int c1 )
{
	dest.value = reg1.value - c1;
}

void MIPS32_Operations::op_li( MIPS32Register & dest, MIPS32Register & reg1 )
{
	dest.value = reg1.value;
}

void MIPS32_Operations::op_li( MIPS32Register & dest, const int c1 )
{
	dest.value = c1;
}

void MIPS32_Operations::op_mov( MIPS32Register & dest, MIPS32Register & src )
{
	dest.value = src.value;
}

bool MIPS32_Operations::op_beq( MIPS32Register & reg1, MIPS32Register & reg2 )
{
	if( reg1.value == reg2.value )
		return true;

	return false;
}
