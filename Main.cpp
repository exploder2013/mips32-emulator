#include "Emulator.h"
#include <iostream>
#include <chrono>

int main()
{
	MIPS32_Emulator loader = MIPS32_Emulator( "code.txt" );

	auto startTime = std::chrono::high_resolution_clock::now();

	loader.execute();

	auto endTime = std::chrono::high_resolution_clock::now();
	auto diffirence = std::chrono::duration_cast<std::chrono::milliseconds>( endTime - startTime );

	printf( "\n-------------------------------------------------------------\n" );
	printf( "Registers of a finished program\n" );
	printf( "-------------------------------------------------------------\n" );
	for( auto reg : loader.getRegisters() )
	{
		printf( "Name: %s \t| \tvalue: %d\n", reg.name.c_str(), reg.value );
	}
	printf( "\n-------------------------------------------------------------\n" );
	printf( "Variables of a finished program\n" );
	printf( "-------------------------------------------------------------\n" );

	for( auto var : loader.getVariables() )
	{
		printf( "Name: %s \t| \tvalue: %d \t| \tSize: %d\n", var->label.c_str(), var->value, var->arraySize );
	}

	printf( "\nDuration: %d milliseconds\n", (int)diffirence.count() );

	
	printf( "\nPress any key to exit.\n");

	int exit = 0;
	std::cin >> exit;

	return 0;
}