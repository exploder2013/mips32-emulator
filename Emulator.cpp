#include "Emulator.h"
#include "Operations.h"

MIPS32_Emulator::MIPS32_Emulator()
{
	PC = 0;
}

MIPS32_Emulator::MIPS32_Emulator( std::string file )
{
	MIPS32_FileLoader loader( file );
	add( loader.getProcessedFileLines() );
}

MIPS32_Emulator::~MIPS32_Emulator()
{
	/* free dynamically allocated memory */
	for( auto variable : variables )
		delete variable;
}

bool MIPS32_Emulator::setup()
{
	MIPS32_Encoder encoder;

	/* register all data variables so we can use them */
	{
		static bool dataSection = false;
		static bool textSectionFound = false;

		for( auto code : codeToExecute )
		{
			auto instr = encoder.encodeInstruction( code );
			auto arguments = std::get<1>( instr );

			/* we found data section, register the variables */
			if( std::get<0>( instr ) == MIPS32InstructionType::DATA )
			{
				dataSection = true;

				continue;

				/* we don't have to do anything at text section */
			} else if( std::get<0>( instr ) == MIPS32InstructionType::TEXT )
			{
				dataSection = false;
				textSectionFound = true;
			}

			/* register all data variables */
			if( dataSection )
			{
				auto splitArgs = split( arguments, ' ' );

				/* reserve some space for the variable */
				if( std::get<0>( instr ) == MIPS32InstructionType::SPACE )
				{
					/* handle error */
					if( splitArgs.size() != 2 )
					{
						printf( "Data variable defined incorrectly! Has %d args.\n", splitArgs.size() );
						return false;

						/* register data variable */
					} else {

						/* these are deleted at the deconstructor */
						MIPS32Variable* variable = new MIPS32Variable();

						/* set it's identifier */
						variable->label = splitArgs.at( 0 );

						/* try safely converting the data variable from string to int */
						try
						{
							variable->byteArray = new char[ stoi( splitArgs.at( 1 ) ) ];
							variable->arraySize	= stoi( splitArgs.at( 1 ) );
						} catch( const std::exception& ex )
						{
							printf( "Couldn't convert string to number for data variable! Exception: %s\n", ex.what() );

							delete variable;
							return false;
						}

						/* put the data variable in our vector */
						variables.push_back( variable );
					}
				} else {
					/* handle error */
					if( splitArgs.size() != 2 )
					{
						printf( "Data variable defined incorrectly! Has %d args.\n", splitArgs.size() );
						return false;

						/* register data variable */
					} else {

						/* these are deleted at the deconstructor */
						MIPS32Variable* variable = new MIPS32Variable();

						/* set it's identifier */
						variable->label = splitArgs.at( 0 );

						/* try safely converting the data variable from string to int */
						try
						{
							variable->value = stoi( splitArgs.at( 1 ) );
						} catch( const std::exception& ex )
						{
							printf( "Couldn't convert string to number for data variable! Exception: %s\n", ex.what() );

							delete variable;
							return false;
						}

						/* put the data variable in our vector */
						variables.push_back( variable );
					}
				}
			}
		}

		if( !textSectionFound )
		{
			printf( "Code contains no .text section!\n" );
			return false;
		}

	}

	/* used to simulate at what position PC would be when reaching the label */
	{
		unsigned int virtualPC = 0;
		for( auto code : codeToExecute )
		{
			auto instr = encoder.encodeInstruction( code );
			auto arguments = getArguments( std::get<1>( instr ) );

			if( std::get<0>( instr ) == MIPS32InstructionType::LABEL )
			{
				std::string labelName = std::get<1>( instr );

				if( !labelName.empty() )
				{
					MIPS32Label label( labelName, virtualPC );
					labels.push_back( label );
				} else {
					printf( "Error - empty label name at %d PC \n", virtualPC );
					return false;
				}

			}

			virtualPC++;
		}
	}

	return true;
}

void MIPS32_Emulator::add( const std::string & code )
{
	codeToExecute.push_back( code );
}

void MIPS32_Emulator::add( const std::vector<std::string> code )
{
	MIPS32_Encoder encoder;

	/* reserve the size beforehand to save performance */
	codeToExecute.reserve( code.size() );

	/* copy the code vector to our vector */
	for( auto line : code )
	{
		codeToExecute.push_back( line );
		auto instr = encoder.encodeInstruction( line );

		//printf( "Line: %s\nInstruction: %s\n", line.c_str(), std::get<1>( instr ).c_str() );
	}

	/* register all the labels, so we don't have to search for them at runtime */
	if( setup() == false )
	{
		printf( "Setup failed! Cannot execute code.\n" );
		codeToExecute.clear();

		return;
	}
}

bool MIPS32_Emulator::execute()
{
	MIPS32_Encoder encoder;

	while( PC != codeToExecute.size() )
	{
		std::string code	= codeToExecute.at( PC );
		std::string label	= "";

		auto instr		= encoder.encodeInstruction( code );
		auto arguments	= getArguments( std::get<1>( instr ) );

		static MIPS32Register*	localRegisters[ 3 ];
		static int				values[ 3 ] = { 0 };

		switch( std::get<0>( instr ) )
		{
		case MIPS32InstructionType::ADD:
		{
			bool hasConstants = false;
			hasConstants = loadRegisters( arguments, localRegisters, values, label );

			if( hasConstants == false ) {
				op_add( *localRegisters[ 0 ], *localRegisters[ 1 ], *localRegisters[ 2 ] );
			} else {
				op_add( *localRegisters[ 0 ], *localRegisters[ 1 ], values[ 0 ] );
			}

			break;
		}
		case MIPS32InstructionType::SUB:
		{
			bool hasConstants = false;
			hasConstants = loadRegisters( arguments, localRegisters, values, label );

			if( hasConstants == false ) {
				op_sub( *localRegisters[ 0 ], *localRegisters[ 1 ], *localRegisters[ 2 ] );
			} else {
				op_sub( *localRegisters[ 0 ], *localRegisters[ 1 ], values[ 0 ] );
			}

			break;
		}
		case MIPS32InstructionType::LI:
		{
			bool hasConstants = false;
			hasConstants = loadRegisters( arguments, localRegisters, values, label );

			if( hasConstants == false ) {
				op_li( *localRegisters[ 0 ], *localRegisters[ 1 ] );
			} else {
				op_li( *localRegisters[ 0 ], values[ 0 ] );
			}

			break;
		}
		case MIPS32InstructionType::BEQ:
		{
			loadRegisters( arguments, localRegisters, values, label );

			if( op_beq( *localRegisters[ 0 ], *localRegisters[ 1 ] ) )
			{
				/* error handling */
				if( !jumpToLabel( label ) )
				{
					return false;
				}
			}

			break;
		}
		case MIPS32InstructionType::J:
		{
			/* error handling */
			if( !jumpToLabel( arguments.at( 0 ) ) )
			{
				printf( "Invalid jump label: %s\n", arguments.at( 0 ).c_str() );
				return false;
			}

			break;
		}
		case MIPS32InstructionType::MOVE:
		{
			loadRegisters( arguments, localRegisters, values, label );
			op_mov( *localRegisters[0], *localRegisters[1] );

			break;
		}
		case MIPS32InstructionType::LW:
		{
			loadRegisters( arguments, localRegisters, values, label );
			auto variable = getVariable( label );

			/* handle error */
			if( variable == nullptr || variable->label.empty() )
			{
				printf( "Invalid variable label referenced (%s)!\n", label.c_str() );
				return false;
			} else {
				localRegisters[ 0 ]->value = variable->value;
			}

			break;
		}
		case MIPS32InstructionType::SW:
		{
			loadRegisters( arguments, localRegisters, values, label );
			
			bool success = setVariable( label, localRegisters[0]->value );
			if( !success )
			{
				printf( "Invalid variable label referenced (%s)!\n", label.c_str() );
				return false;
			}

			break;
		}
		case MIPS32InstructionType::SYSCALL:
		{
			/* register used to decide what kind of a syscall it is */
			auto reg = getRegister( "v0" );

			switch( reg->value )
			{
			/* print */
			case 1:
			{
				auto output = getRegister( "a0" );
				printf( "Output: %d \t| \tPC: %d\n", output->value, PC );

				break;
			}
			/* get input from user */
			case 5:
			{
				auto input = getRegister( "a0" );
				printf( "Enter input: " );

				/* should revisit this and check if input == int */
				std::cin >> input->value;

				break;
			}
			/* exit program */
			case 10:
			{
				printf( "SYSCALL: EXIT \t| \tPC: %d\n", PC );
				return true;
			}
			default:
			{
				printf( "Unsupported SYSCALL: %d\n", reg->value );
				return false;
			}
			}

			break;
		}
		default:
		{
			/* data variables go through here too */
			//printf( "Error: Unsupported instruction!\n" );
			//printf( "Line: %s\n", code.c_str() );

			break;
		}
		}

		/* increment the program counter */
		PC++;
	}

	return false;
}

std::vector<MIPS32Variable*> MIPS32_Emulator::getVariables()
{
	return this->variables;
}

std::vector<std::string> MIPS32_Emulator::getArguments( const std::string & code )
{
	std::vector< std::string >	arguments;
	std::string					instrType;


	if( code.empty() )
	{
		return arguments;
	}

	/* split the string */
	auto args = split( code, ' ' );

	/* empty string */
	if( args.size() == 0 )
	{
		return arguments;
	}
	instrType = args.at( 0 );

	/* 3 arguments */
	if( instrType == "R" )
	{
		/* error */
		if( args.size() != 4 )
		{
			return arguments;
		}

		for( unsigned int i = 1; i < 4; i++ )
			arguments.push_back( args[ i ] );
	/* 2 arguments */
	} else if( instrType == "I" )
	{
		/* error */
		if( args.size() != 3 )
		{
			return arguments;
		}

		for( unsigned int i = 1; i < 3; i++ )
			arguments.push_back( args[ i ] );
	/* 1 argument */
	} else if( instrType == "S" )
	{
		/* error */
		if( args.size() != 2 )
		{
			return arguments;
		}

		for( unsigned int i = 1; i < 2; i++ )
			arguments.push_back( args[ i ] );
	}

	return arguments;
}

std::vector<std::string> MIPS32_Emulator::split( const std::string & str, const char delimiter )
{
	std::vector<std::string> v_split;
	unsigned int pos = 0, prevPos = 0;

	while( ( pos = str.find_first_of( delimiter, prevPos ) ) != -1 )
	{
		std::string match = str.substr( prevPos, pos - prevPos );
		v_split.push_back( match );

		prevPos = pos + 1;
	}

	/* get the last argument */
	if( prevPos != str.length() )
		v_split.push_back( str.substr( prevPos, str.length() - prevPos ) );

	return v_split;
}

bool MIPS32_Emulator::loadRegisters( const std::vector< std::string >& arguments, MIPS32Register* localRegisters[ 3 ], int values[ 3 ], std::string& label )
{
	unsigned int	argIndex		= 0;
	bool			hasConstrants	= false;

	while( argIndex < arguments.size() )
	{
		std::string argument = arguments[ argIndex ];

		/* register */
		if( argument.find( '$' ) != std::string::npos )
		{
			/* this will fail with null dereference if the provided register is invalid */
			try
			{
				switch( argIndex )
				{
				case 0:
					localRegisters[ 0 ] = getRegister( argument.substr( 1, 2 ) );
					break;
				case 1:
					localRegisters[ 1 ] = getRegister( argument.substr( 1, 2 ) );
					break;
				case 2:
					localRegisters[ 2 ] = getRegister( argument.substr( 1, 2 ) );
					break;
				}
			} catch( std::exception ex )
			{
				printf( "null dereference exception: %s!\n", ex.what() );
				return false;
			}
			/* memory label/constant */
		} else {
			/* maybe it is a label? If one of the characters is alphabethic, then it has to be */
			if( isalpha( argument.at( 0 ) ) )
			{
				label = argument;
			} else {
				hasConstrants = true;

				try
				{
					switch( argIndex )
					{
					case 0:
						values[ 0 ] = std::stoi( argument );
						break;
					case 1:
						values[ 0 ] = std::stoi( argument );
						break;
					case 2:
						values[ 0 ] = std::stoi( argument );
						break;
					}
				} catch( std::exception ex )
				{
					printf( "stoi exception: %s!\n", ex.what() );
					return false;
				}
			}
		}

		argIndex++;
	}

	return hasConstrants;
}

bool MIPS32_Emulator::jumpToLabel( const std::string & label )
{
	if( label.empty() )
		return false;

	for( auto l : labels )
	{
		if( l.label == label )
		{
			/* adjust PC accordingly */
			PC = l.PCLocation;
			return true;
		}
	}

	return false;
}

MIPS32Variable* MIPS32_Emulator::getVariable( const std::string & label )
{
	if( label.empty() )
		return nullptr;

	for( auto variable : variables )
	{
		if( variable->label == label )
			return variable;
	}

	return nullptr;
}

bool MIPS32_Emulator::setVariable( const std::string & label, int newValue )
{
	if( label.empty() )
		return false;

	for( auto variable : variables )
	{
		if( variable->label == label )
		{
			variable->value = newValue;
			return true;
		}
			
	}

	return false;
}
