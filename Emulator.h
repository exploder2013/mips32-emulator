#pragma once

#include <string>
#include <vector>

/* used to get input from user */
#include <iostream>

#include "Registers.h"
#include "Encoder.h"
#include "FileLoader.h"
#include "Operations.h"

typedef struct _MIPS32Instruction
{
	_MIPS32Instruction( unsigned int _instruction )
	{
		instruction.instruction = _instruction;
	}

	/* might improve on this later */
	typedef union _instr
	{
		char			opcodes[ 4 ];
		unsigned int	instruction;
	} instr;

	instr instruction;
	
} MIPS32Instruction;

typedef struct _MIPS32Label
{
	_MIPS32Label() {};
	_MIPS32Label( std::string _label, int _labelIndex )
	{
		label = _label;
		PCLocation = _labelIndex;
	}

	std::string label		= "";
	int			PCLocation	= -1;

} MIPS32Label;

typedef struct _MIPS32Variable
{
	_MIPS32Variable() {};

	_MIPS32Variable( std::string _label, int _value )
	{
		label = _label;
		value = _value;
	}

	_MIPS32Variable( std::string _label, unsigned int spaceSize )
	{
		label		= _label;

		arraySize	= spaceSize;
		byteArray	= new char[ spaceSize ];
	}

	std::string label	= "";
	
	/* used for .space specifier */
	char*		byteArray	= 0;
	int			arraySize	= 0;

	int			value		= 0;

} MIPS32Variable;

class MIPS32_Emulator : public MIPS32_Registers, MIPS32_Operations
{
public:
	MIPS32_Emulator();
	MIPS32_Emulator( std::string file );
	~MIPS32_Emulator();
	
	bool setup();

	void add( const std::string& code );
	void add( const std::vector<std::string> code );

	bool execute();

	std::vector< MIPS32Variable* > getVariables();
private:
	std::vector< std::string > getArguments( const std::string& code );
	std::vector< std::string > split( const std::string& str, const char delimiter );

	bool					   loadRegisters( const std::vector< std::string >& arguments, MIPS32Register* localRegisters[ 3 ], int values[ 3 ], std::string& label );
	bool					   jumpToLabel( const std::string& label );

	MIPS32Variable*			   getVariable( const std::string& label );
	bool					   setVariable( const std::string& label, int newValue );

private:
	std::string lastExecutedLine;
	std::string executedLine;
	std::string nextLineToExecute;

	std::vector< std::string >			codeToExecute;

private:
	std::vector< MIPS32Instruction >	instructions;
	std::vector< MIPS32Label >			labels;

	/* variables are pointers because they can be modified by the code */
	/* could be later reimplemented as a class that handles DWORD's, FLOATS, etc...*/
	std::vector< MIPS32Variable* >		variables;

	/* Program counter */
	unsigned int						PC; 
};