#include "Encoder.h"

const std::string supportedInstructions[] = { "add","sub","slt","and","or","syscall","beq","lw","sw","li","move","j", "space" };

MIPS32_Encoder::MIPS32_Encoder()
{
}

MIPS32_Encoder::~MIPS32_Encoder()
{
}

std::tuple<MIPS32InstructionType, std::string  > MIPS32_Encoder::encodeInstruction( const std::string& _instruction )
{
	std::string				newInstr	= _instruction;
	std::string				instrType	= "";		
	std::string				arguments	= "";

	if( _instruction.empty() )
		return std::make_tuple( MIPS32InstructionType::EMPTY, instrType );

	/* convert the passed instruction to lower case */
	for( unsigned int i = 0; i < newInstr.length(); i++ )
		newInstr[ i ] = static_cast<char>( std::tolower( newInstr[ i ] ) );

	/* data section */
	if( newInstr.find( ".data" ) != std::string::npos )
	{
		return std::make_tuple( MIPS32InstructionType::DATA, ".data" );
	}

	/* code section */
	if( newInstr.find( ".text" ) != std::string::npos )
	{
		return std::make_tuple( MIPS32InstructionType::TEXT, ".text" );
	}

	/* code section */
	if( newInstr.find( "main" ) != std::string::npos )
	{
		return std::make_tuple( MIPS32InstructionType::MAIN, "main" );
	}

	/* label */
	if( newInstr.find( ":" ) != std::string::npos )
	{
		/* has to be a data definition not a label */
		if( newInstr.find( "." ) != std::string::npos )
		{
			if( newInstr.find( "space" ) != std::string::npos )
				return std::make_tuple( MIPS32InstructionType::SPACE, getData( newInstr ) );

			return std::make_tuple( MIPS32InstructionType::LABEL_DATA, getData( newInstr ) );
		} else {
			/* first let's remove all the spaces after label */
			int pos = newInstr.find_first_of( ':' );

			while( newInstr.at( pos - 1 ) == ' ' )
				pos--;

			return std::make_tuple( MIPS32InstructionType::LABEL, newInstr.substr( 0, pos ) );
		}
	}

	auto type	= getInstructionType( newInstr );
	arguments	= getArguments( newInstr, std::get<1>( type ) );

	std::get<1>( type ) += " ";
	std::get<1>( type ) += arguments;

	return type;
}

std::vector< std::tuple< MIPS32InstructionType, std::string > > MIPS32_Encoder::encodeInstructions( const std::vector<std::string>& instructions )
{
	/* reserve memory for the instructions */
	encodedInstructions.reserve( instructions.size() );

	for( auto instr : instructions )
	{
		/* encode it */
		auto encoded = encodeInstruction( instr );
		/* store it */
		encodedInstructions.push_back( encoded );
	}

	return encodedInstructions;
}

std::tuple< MIPS32InstructionType, std::string > MIPS32_Encoder::getInstructionType( const std::string & instruction )
{
	/* R type instructions */
	while( true )
	{
		/* there can be multiple ADD instructions! */
		if( instruction.find( "add" ) != std::string::npos )
		{
			return std::make_tuple( MIPS32InstructionType::ADD, "R" );
		}

		if( instruction.find( "sub" ) != std::string::npos )
		{
			return std::make_tuple( MIPS32InstructionType::SUB, "R" );
		}

		if( instruction.find( "and" ) != std::string::npos )
		{
			return std::make_tuple( MIPS32InstructionType::AND, "R" );
		}

		if( instruction.find( "or" ) != std::string::npos )
		{
			return std::make_tuple( MIPS32InstructionType::OR, "R" );
		}

		if( instruction.find( "slt" ) != std::string::npos )
		{
			return std::make_tuple( MIPS32InstructionType::SLT, "R" );
		}

		if( instruction.find( "beq" ) != std::string::npos )
		{
			return std::make_tuple( MIPS32InstructionType::BEQ, "R" );
		}

		break;
	}

	/* I type instructions */
	while( true )
	{
		/* there can be multiple ADD instructions! */
		if( instruction.find( "li" ) != std::string::npos )
		{
			return std::make_tuple( MIPS32InstructionType::LI, "I" );
		}

		if( instruction.find( "la" ) != std::string::npos )
		{
			return std::make_tuple( MIPS32InstructionType::LA, "I" );
		}

		if( instruction.find( "lw" ) != std::string::npos )
		{
			return std::make_tuple( MIPS32InstructionType::LW, "I" );
		}

		if( instruction.find( "sw" ) != std::string::npos )
		{
			return std::make_tuple( MIPS32InstructionType::SW, "I" );
		}

		if( instruction.find( "move" ) != std::string::npos )
		{
			return std::make_tuple( MIPS32InstructionType::MOVE, "I" );
		}

		break;
	}

	/* special type instructions */
	while( true )
	{
		if( instruction.find( "j" ) != std::string::npos )
		{
			return std::make_tuple( MIPS32InstructionType::J, "S" );
		}

		if( instruction.find( "space" ) != std::string::npos )
		{
			return std::make_tuple( MIPS32InstructionType::SPACE, "S" );
		}


		break;
	}

	/* no argument functions */
	while( true )
	{
		if( instruction.find( "syscall" ) != std::string::npos )
		{
			return std::make_tuple( MIPS32InstructionType::SYSCALL, "SYSCALL" );
		}

		break;
	}

	return std::make_tuple( MIPS32InstructionType::SLT, "INVALID" );
}

std::string MIPS32_Encoder::getArguments( const std::string & instruction, const std::string & type )
{
	std::string arguments;

	/* R Type instruction (3 arguments) */
	if( type == "R" )
	{
		arguments = getArgumentsFromCount( instruction, 3 );
	} else if( type == "I" )
	{
		arguments = getArgumentsFromCount( instruction, 2 );
	} else if( type == "S" )
	{
		arguments = getArgumentsFromCount( instruction, 1 );
	}

	return arguments;
}

std::string MIPS32_Encoder::getData( const std::string & instruction )
{
	std::string arguments;

	unsigned int pos = 0;

	/* get data label */
	if( ( pos = instruction.find_first_of( ":" ) ) != std::string::npos )
	{
		/* remove all the spaces after label */
		while( instruction.at( pos - 1 ) == ' ' )
			pos--;

		arguments += instruction.substr( 0, pos );
	}

	/* get the variables init number */
	if( ( pos = instruction.find( ".word" ) ) != std::string::npos )
	{
		arguments += instruction.substr( pos + 5, instruction.length() - pos - 1 );
	} else if( ( pos = instruction.find( ".space" ) ) != std::string::npos )
	{
		arguments += instruction.substr( pos + 6, instruction.length() - pos - 1 );
	}

	return arguments;
}

std::string MIPS32_Encoder::getArgumentsFromCount( const std::string & instruction, unsigned int argCount )
{
	std::string arguments;
	unsigned int pos = 0, counter = 0;

	/* find first space (after the instruction itself) */
	pos = instruction.find_first_of( " ", 1 );

	/* invalid instruction */
	if( pos == -1 )
	{
		_CrtDbgBreak();
	} else {
		pos++;
		
		/* if argCount is 1, then just get the argument */
		if( argCount == 1 )
		{
			while( instruction.at( pos ) == ' ' )
				pos++;

			arguments += instruction.substr( pos, instruction.length() - pos );
			return arguments;
		}

		/* R type instructions have 3 arguments */
		while( pos != instruction.length() && counter < argCount )
		{
			/* register */
			if( instruction.at( pos ) == '$' )
			{
				counter++;

				if( counter == 3 ) {
					arguments += instruction.substr( pos, 3 );
					break;
				} else {
					arguments += instruction.substr( pos, 3 ) += " ";
				}


				pos += 3;
				continue;
				/* next argument */
			} else if( instruction.at( pos ) == ',' )
			{
				pos++;
				/* if it is space, then increment the position counter */
				while( instruction.at( pos ) == ' ' )
					pos++;

				/* register */
				if( instruction.at( pos ) == '$' )
				{
					counter++;

					if( counter == 3 ) {
						arguments += instruction.substr( pos, 3 );
						break;
					} else {
						arguments += instruction.substr( pos, 3 ) += " ";
					}

					pos += 3;
					continue;
					/* constant or memory label */
				} else {
					counter++;

					/* last instruction */
					if( counter == argCount )
					{
						arguments += instruction.substr( pos, instruction.length() - pos );
						break;

						/* somewhere in middle */
					} else {
						int nextArg = instruction.find_first_of( ',', pos );

						/* next argument has not been found! */
						if( nextArg == -1 )
						{
							_CrtDbgBreak();
						} else {

							/* skip the spaces */
							while( instruction.at( nextArg - 1 ) == ' ' )
								nextArg--;
						}

						/* get the constant without spaces */
						arguments += instruction.substr( pos, instruction.length() - nextArg - 1 ) += " ";
						pos += instruction.length() - nextArg - 1;

						continue;
					}

				}
			}

			pos++;
		}
	}

	/* invalid amount of arguments */ 
	if( counter != argCount )
	{
		_CrtDbgBreak();
	}

	return arguments;
}
