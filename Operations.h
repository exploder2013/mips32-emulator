#pragma once

#include "Registers.h"

class MIPS32_Operations
{
public:
	MIPS32_Operations();
	~MIPS32_Operations();

	//static MIPS32_Operations* getInstance();

	void	op_add( MIPS32Register& dest, const MIPS32Register& reg1, const MIPS32Register& reg2 );
	void	op_add( MIPS32Register& dest, const MIPS32Register& reg1, const int c1 );

	void	op_sub( MIPS32Register& dest, const MIPS32Register& reg1, const MIPS32Register& reg2 );
	void	op_sub( MIPS32Register& dest, const MIPS32Register& reg1, const int c1 );

	void	op_li( MIPS32Register& dest, MIPS32Register& reg1 );
	void	op_li( MIPS32Register& dest, const int c1 );

	void	op_mov( MIPS32Register& dest, MIPS32Register& src );

	bool	op_beq( MIPS32Register& reg1, MIPS32Register& reg2 ); // cmp reg1 == reg2
private:
	//static MIPS32_Operations* instance;
};