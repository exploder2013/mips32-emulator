#pragma once

#include <string>
#include <tuple>
#include <vector>
#include <cctype>

#include "Registers.h"

typedef enum _MIPS32InstructionType
{
	ADD 	= 0b100000,
	SUB 	= 0b100010,
	SLT 	= 0b101010,
	AND 	= 0b100100,
	OR 		= 0b100101,
	SYSCALL = 0b001100,
	BEQ 	= 0b000100,
	LW 		= 0b100011,
	SW 		= 0b101011,
	LI 		= 0b100001,
	LA		= 0xdeadbeef,
	MOVE 	= 0b000110,
	J 		= 0b000010,

	/* these enums are not a part of the instruction set */
	LABEL	= 1337, 
	INVALID = 1338,
	EMPTY	= 1339,
	DATA	= 1340,
	TEXT	= 1341,
	SPACE	= 1342,
	MAIN	= 1343,
	LABEL_DATA = 1344,
} MIPS32InstructionType;

extern const std::string supportedInstructions[];

class MIPS32_Encoder
{
public:
	MIPS32_Encoder();
	~MIPS32_Encoder();

	std::tuple< MIPS32InstructionType, std::string >							encodeInstruction( const std::string& _instruction );
	std::vector< std::tuple< MIPS32InstructionType, std::string > >				encodeInstructions( const std::vector<std::string>& instructions );

private:
	std::tuple< MIPS32InstructionType, std::string > getInstructionType( const std::string& instruction );
	std::string										 getArguments( const std::string& instruction, const std::string& type );
	std::string										 getData( const std::string& instruction );

	std::string										 getArgumentsFromCount( const std::string& instruction, unsigned int argCount );

private:
	std::vector< std::tuple< MIPS32InstructionType, std::string > > encodedInstructions;
};