#pragma once

#include <string>
#include <array>
#include <cctype>

typedef struct _MIPS32Register
{
	/* The name for the actual register (like $t0, $t1, ... ) */
	std::string name = "";

	/* The value that the register is currently holding */
	int			value = 0;

} MIPS32Register, *PMIPS32Register;

class MIPS32_Registers
{
public:
	MIPS32_Registers ();
	~MIPS32_Registers();

	/* provide custom starting register values. */
	MIPS32_Registers ( const MIPS32Register _registers[ 32 ] );
	
	/* getters */
	int					getRegisterValue( const std::string& registerName );

	MIPS32Register*		getRegister		( const std::string& registerName );
	MIPS32Register*		getRegister		( const int& registerIndex );
	std::array< MIPS32Register, 32 >	getRegisters();

	/* might implement setters later */
private:
	MIPS32Register*		findRegister( const std::string name );

private:
	std::array< MIPS32Register, 32 > registers;
};