#pragma once

#include <fstream>
#include <stdio.h>
#include <vector>
#include <string>

/*
	MIPS32_FileLoader - 

	Purpose:	Load the file provided by the fileLocation or openFile method.
				And clean the file, so it only contains code that other classes can
				rely on without doing any extra formatting of the content.
*/

class MIPS32_FileLoader
{
public:
	MIPS32_FileLoader ( const std::string& fileLocation );
	MIPS32_FileLoader ( );
	~MIPS32_FileLoader( );

	std::vector< std::string >  getProcessedFileLines();

	bool						openFile( const std::string& path );
	void						startProcessing();

private:
	void processFile();

private:
	bool		isComment		 ( const std::string& line );
	std::string remoteWhitespace ( const std::string& line );
	std::string parseCode	     ( const std::string& line );

	std::vector<std::string> splitCode ( const std::string& line );

private:

	#pragma warning( disable : 4239 )
	std::ifstream&				file = std::ifstream();
	#pragma warning( default : 4239 )

	std::vector< std::string >	processedLines;
};